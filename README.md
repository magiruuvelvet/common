# Common files

</br>

## Docs

#### Uncategorized

 - [My software versioning scheme](./docs/Software Versioning Scheme.md)

#### Coding Styles

 - [My very unique C++ coding style](./docs/Coding Styles/C++.md)
 - [My very special and unique JavaScript coding style; and why I hate the common style everyone else is using](./docs/Coding Styles/JavaScript.md)

</br></br>

## Config files

*nothing special at the moment, will add some in the future*

</br></br>

## Resources

#### [Bash](./resources/Bash)

 - [My complex and feature rich bash prompt](./resources/Bash/prompt.sh) | [Screenshots](./resources/Bash/prompt-screenshots)


Make sure you also checkout this useful collection of utilities from my other repository -> [miscs](https://github.com/GhettoGirl/miscs)

</br></br>

## Templates

#### Scripts

 - ArchLinux [PKGBUILD](./templates/ArchLinux/PKGBUILD) and [`.install`](./templates/ArchLinux/PKGBUILD.install) script
