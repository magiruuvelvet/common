#!/usr/bin/env node

const version = new (require('./version.js'));

console.log("App package.json:");
console.log("Version:     %s", version.version());
console.log(" -main:      %d", version.main());
console.log(" -progress:  %d", version.progress());
console.log(" -revision:  %d", version.revision());
console.log("");

console.log("Test 1: (expected: works)");
const test1 = new (require('./version.js'))('tests/1.json');
console.log("Version:     %s", test1.version());
console.log(" -main:      %d", test1.main());
console.log(" -progress:  %d", test1.progress());
console.log(" -revision:  %d", test1.revision());
console.log("");

console.log("Test 2: (expected: works)");
const test2 = new (require('./version.js'))('tests/2.json');
console.log("Version:     %s", test2.version());
console.log(" -main:      %d", test2.main());
console.log(" -progress:  %d", test2.progress());
console.log(" -revision:  %d", test2.revision());
console.log("");

console.log("Test 3: (expected: fails)");
const test3 = new (require('./version.js'))('tests/3.json');
console.log("Version:     %s", test3.version());
console.log(" -main:      %d", test3.main());
console.log(" -progress:  %d", test3.progress());
console.log(" -revision:  %d", test3.revision());
console.log("");

console.log("Test 4: (expected: fails)");
const test4 = new (require('./version.js'))('tests/4.json');
console.log("Version:     %s", test4.version());
console.log(" -main:      %d", test4.main());
console.log(" -progress:  %d", test4.progress());
console.log(" -revision:  %d", test4.revision());
console.log("");

console.log("Test 5: (expected: fails)");
const test5 = new (require('./version.js'))('tests/5.json');
console.log("Version:     %s", test5.version());
console.log(" -main:      %d", test5.main());
console.log(" -progress:  %d", test5.progress());
console.log(" -revision:  %d", test5.revision());
console.log("");
