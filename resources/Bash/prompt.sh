##################################################################################
##################################################################################
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ##
##################################################################################
##################################################################################
 #                                                                              #
 # GhettoGirl's Ultimate Bash Prompt                           [version 3.7.5]  #
 #                                                                              #
##################################################################################
##################################################################################
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ##
##################################################################################
##################################################################################

### README: copy and paste this into your .bashrc
###   If you get any errors try to rearrange some stuff first, before reporting
###   bugs please :)
###   You can also 'source' this, but for efficiency reasons, please don't do that!
###   See at the bottom of this file and you will understand.

### NOTES =======================================================================
###
###  This is a very advanced bash prompt, it is not recommended to use this on
###  slow machines!!
###
###  REQUIRES AT LEAST GNU/BASH 4.2 or later, or you get syntax errors!!
###  Recommended: A TERMINAL EMULATOR WHICH SUPPORTS TRUE COLOR (16 MILLION COLORS)
###
###  The performance in zsh is very bad and I don't know why.
###  If zsh wouldn't take like 1 minute to load the prompt, I would have switched
###  to zsh long time ago :(
###
### =============================================================================

###
### Core functionality
###

# Create a temporary directory in memory to work with complex things, because
# bash's variable scoping is quite "dumb", therefore we need files instead of
# variables.
# Do avoid useless writes to physical disks, use the shared memory device
#   --- /dev/shm
# ────────────────────────────────────────────────────────────────────────────────
# Since we are using the RAM, this directories are lost on reboot anyway,
# but still do a clean up to free some memory.
# ────────────────────────────────────────────────────────────────────────────────
# CLEANUP TASKS: override 'source' command and trap bash exit
if [ ! -z "$PS1" ]; then
    # only for interactive shells, otherwise the tty command triggers an error and the whole file is skipped
    # this is because the login session is not piped to an shell (tty or pts)
    # this is only required for interactive shells anyway
export BASHRC_TMPFS="$(mktemp -d /dev/shm/bashrc_$(whoami)_$(tty | tr '/' '_')_tmpfs_XXXXXXXXX)"
function bashrc_clean_tmpfs() { rm -rf "$BASHRC_TMPFS"; }
function source() {
    # if the 'source' command fails or no arguments were provided, reload this .bashrc
    if (( $# < 1 )); then
        echo -e "\e[1msource:\e[0m \e[0m\e[0;31mno arguments provided\e[0m" >&2
        echo -e "\e[1msource:\e[0m usage: \e[1m\e[38;5;91msource\e[0m \e[4mfilename\e[0m \e[3m[arguments]\e[0m"
        return 1
    else
        bashrc_clean_tmpfs
        if ! builtin source "$@"; then
            echo -e "\e[1msource:\e[0m \e[0;31merror loading new file, restoring...\e[0m" >&2
            builtin source "$HOME/.bashrc"
            return 1
        else return 0; fi
    fi
}
trap bashrc_clean_tmpfs EXIT
fi

# check the available terminal space after every command (for optical reasons)
shopt -s checkwinsize

###
### Prompt
###

###  => 256 color support is enabled (besides most modern terminal emulators supports 16 million colors)
###  => a super-duper fancy and dynamic bash prompt is created (over 5 lines with lots of info in it ─ in full color)
###           × requires at least 80 columns to work, 100 are highly recommended!!!
###           × design continues at $PS2 to match the end of $PS1
###           × the first line is always empty (newline) ─ in case last program forgets to insert a newline to avoid "uglyness"
###           × colored and matching $PS3 in case a shell script uses 'select' without setting $PS3 internally
###        ─ the info of $PS1 ─
###           × users fullname from /etc/passwd
###           × machines hostname
###           × operating system details
###           × current time (not updated dynamically ─ bash limitation) | NOTE: zsh is too slow for "THIS" prompt :/
###           × last exit status or signal name if it was a signal
###           × number of inodes in current working directory (excluding hidden ones .*)
###           × current working directory (dynamically formatted, like bold slash and nice line wrapping)
###           × additional message of last exit code if it was a signal, bash builtin or if in the builtin database
###           × support for custom messages (last command + exit status)
###           × current running and stopped jobs
###           × terminal type and number, example: tty1, tty2 for VT and pts/1, pts/2 for graphical terminals
###           × a minimal git status monitor (probes for git repo on directory change ─ only visible if in git repo)
###           × input prompt in its own line
###  => a super-duper fancy and dynamic xtrace ($PS4) line is created (full color and informative)
###           × more than 150 columns are highly recommended!

# Color Table
export TERM=xterm-256color
export TERM=screen-256color

# BEGIN of $PS1, $PS2, $PS3, $PS4
# only when interactive ─ otherwise this may 'cause issues during login into a graphical session
if [ ! -z "$PS1" ]; then

# Write last command to file
function bashrc_write_lastcommand() {
    # Has minor issues with custom $PROMPT_COMMAND, need to add them here ;(
    if [[ "$BASH_COMMAND" != "history -a" && \
          "$BASH_COMMAND" != "history -c" && \
          "$BASH_COMMAND" != "history -r" ]]; then
        local LSTCMD="$BASH_COMMAND" # need this line,
                                     # because $BASH_COMMAND is already updated if you take a look at the next line
                                     # for security issues only store the command itself without its arguments
        # Command: <super user exec> ─ store with actual command
        if [[ "$LSTCMD" == sudo\ * || \
              "$LSTCMD" == su\ * || \
              "$LSTCMD" == gksu\ * || \
              "$LSTCMD" == gksudo\ * || \
              "$LSTCMD" == kdesu\ * || \
              "$LSTCMD" == kdesudo\ * ]]; then
            printf "%s" "$(echo "$LSTCMD" | awk '{printf $1; printf " "; printf $2}')" > "$BASHRC_TMPFS/lstcmd"
        else
            printf "%s" "$(echo "$LSTCMD" | awk '{print $1}')" > "$BASHRC_TMPFS/lstcmd"
        fi
        LSTCMD="" # clear variable
    fi
    
    # for performance reasons add to end of file: [ ! -z "$PS1" ] && trap bashrc_write_lastcommand DEBUG
    touch "$BASHRC_TMPFS/lstcmd"
}

# Works together with 'bashrc_get_exitcode', prints custom messages depending on last command and exit status
function bashrc_extended_description_util() {
    local EXIT=$(< "$BASHRC_TMPFS/exitcode")
    local LSTCMD="$(echo "$(< "$BASHRC_TMPFS/lstcmd")" | awk '{printf $1}')"
    
    local MSG_TEXT="" # Message to return (contains escape sequences)
    local MSG_SIZE=0  # Size of message (real size ─ without escape sequences and non-printable characters)
    local MSG_ROOT="@[\e[31mroot\e[0m] " # "Running as root"-prefix
    
    # NOTE: Don't use 'let' in here ─── too slow! ((arithmetic expansion)) is way faster
    
    # Resolve --> <super user exec> and
    # prepend $MSG_ROOT to the message
    # set $LSTCMD to actual command accordingly
    if [[ "$LSTCMD" == "sudo" || \
          "$LSTCMD" == "su" || \
          "$LSTCMD" == "gksu" || \
          "$LSTCMD" == "gksudo" || \
          "$LSTCMD" == "kdesu" || \
          "$LSTCMD" == "kdesudo" ]]; then
            LSTCMD="$(echo "$(< "$BASHRC_TMPFS/lstcmd")" | awk '{printf $2}')"
            MSG_TEXT+="$MSG_ROOT"
            MSG_SIZE=8
    fi
    
    # Command: man
    if [[ "$LSTCMD" == "man" ]]; then
        if (( $EXIT == 16 )); then
            [ "$1" == "size" ] &&  ((MSG_SIZE+=26)) || \
                MSG_TEXT+="\e[1mman:\e[0m \e[3mManual page not found\e[0m"
        fi
    
    # Command: make
    elif [[ "$LSTCMD" == "make" ]]; then
        if (( $EXIT == 0 )); then
            [ "$1" == "size" ] && ((MSG_SIZE+=28)) || \
                MSG_TEXT+="\e[1mmake:\e[0m \e[0;32mdone, everything built\e[0m"
        elif (( $EXIT == 1 )); then
            [ "$1" == "size" ] && ((MSG_SIZE+=29)) || \
                MSG_TEXT+="\e[1mmake:\e[0m \e[0;31mthere were build errors\e[0m"
        elif (( $EXIT == 2 )); then
            [ "$1" == "size" ] && ((MSG_SIZE+=23)) || \
                MSG_TEXT+="\e[1mmake:\e[0m \e[0;31mno makefile found\e[0m"
        fi
    
    # Command: gcc, g++, clang, clang++
    elif [[ "$LSTCMD" == "gcc" || \
            "$LSTCMD" == "g++" || \
            "$LSTCMD" == "clang" || \
            "$LSTCMD" == "clang++" ]]; then
        if (( $EXIT == 0 )); then
            [ "$1" == "size" ] && ((MSG_SIZE+=33)) || \
                MSG_TEXT+="\e[1m[compiler]\e[0m \e[0;32mdone, everything built\e[0m"
        else
            [ "$1" == "size" ] && ((MSG_SIZE+=32)) || \
                MSG_TEXT+="\e[1m[compiler]\e[0m \e[0;31mbuild errors occurred\e[0m"
        fi
    
    # Command: :
    elif [[ "$LSTCMD" == ":" ]]; then
        [ "$1" == "size" ] && ((MSG_SIZE+=27)) || \
            MSG_TEXT+="\e[3mSomething happened, or not?\e[0m"
    
    # Command: <shell builtins>
    elif [[ "$LSTCMD" == "."         || "$LSTCMD" == "alias"    || "$LSTCMD" == "bg"      || "$LSTCMD" == "bind"     || \
            "$LSTCMD" == "break"     || "$LSTCMD" == "builtin"  || "$LSTCMD" == "caller"  || "$LSTCMD" == "case"     || \
            "$LSTCMD" == "cd"        || "$LSTCMD" == "command"  || "$LSTCMD" == "compgen" || "$LSTCMD" == "complete" || \
            "$LSTCMD" == "compopt"   || "$LSTCMD" == "continue" || "$LSTCMD" == "coproc"  || "$LSTCMD" == "declare"  || \
            "$LSTCMD" == "dirs"      || "$LSTCMD" == "disown"   || "$LSTCMD" == "echo"    || "$LSTCMD" == "enable"   || \
            "$LSTCMD" == "eval"      || "$LSTCMD" == "exec"     || "$LSTCMD" == "exit"    || "$LSTCMD" == "export"   || \
            "$LSTCMD" == "false"     || "$LSTCMD" == "fc"       || "$LSTCMD" == "fg"      || "$LSTCMD" == "for"      || \
            "$LSTCMD" == "getopts"   || "$LSTCMD" == "hash"     || "$LSTCMD" == "help"    || "$LSTCMD" == "history"  || \
            "$LSTCMD" == "if"        || "$LSTCMD" == "jobs"     || "$LSTCMD" == "kill"    || "$LSTCMD" == "let"      || \
            "$LSTCMD" == "local"     || "$LSTCMD" == "logout"   || "$LSTCMD" == "mapfile" || "$LSTCMD" == "popd"     || \
            "$LSTCMD" == "printf"    || "$LSTCMD" == "pushd"    || "$LSTCMD" == "pwd"     || "$LSTCMD" == "read"     || \
            "$LSTCMD" == "readarray" || "$LSTCMD" == "readonly" || "$LSTCMD" == "return"  || "$LSTCMD" == "select"   || \
            "$LSTCMD" == "set"       || "$LSTCMD" == "shift"    || "$LSTCMD" == "shopt"   || "$LSTCMD" == "source"   || \
            "$LSTCMD" == "suspend"   || "$LSTCMD" == "test"     || "$LSTCMD" == "time"    || "$LSTCMD" == "times"    || \
            "$LSTCMD" == "trap"      || "$LSTCMD" == "true"     || "$LSTCMD" == "type"    || "$LSTCMD" == "typeset"  || \
            "$LSTCMD" == "ulimit"    || "$LSTCMD" == "umask"    || "$LSTCMD" == "unalias" || "$LSTCMD" == "unset"    || \
            "$LSTCMD" == "until"     || "$LSTCMD" == "wait"     || "$LSTCMD" == "while" ]]; then
        if (( $EXIT == 2 )); then
            [ "$1" == "size" ] && ((MSG_SIZE+=30)) || \
                MSG_TEXT+="\e[38;2;30;171;255mBash:\e[0m \e[31mmisuse of shell builtins\e[0m"
        fi
    
    # Command: service
    elif [[ "$LSTCMD" == "service" ]]; then
        if (( $EXIT == 0 )); then
            [ "$1" == "size" ] && ((MSG_SIZE+=13)) || \
                MSG_TEXT+="\e[1mservice:\e[0m \e[0;32mdone\e[0m"
        else
            [ "$1" == "size" ] && ((MSG_SIZE+=23)) || \
                MSG_TEXT+="\e[1mservice:\e[0m \e[0;31merror occurred\e[0m"
        fi
    
    # Command: apt-get
    elif [[ "$LSTCMD" == "apt-get" ]]; then
        if (( $EXIT == 0 )); then
            [ "$1" == "size" ] && ((MSG_SIZE+=16)) || \
                MSG_TEXT+="\e[1mapt-get:\e[0m \e[0;32msuccess\e[0m"
        elif (( $EXIT == 100)); then # insufficient permissions
            [ "$1" == "size" ] && ((MSG_SIZE+=33)) || \
                MSG_TEXT+="\e[1mapt-get:\e[0m \e[0;38;2;170;76;0minsufficient permissions\e[0m"
        else
            [ "$1" == "size" ] && ((MSG_SIZE+=14)) || \
                MSG_TEXT+="\e[1mapt-get:\e[0m \e[0;31merror\e[0m"
        fi
    
    # === Command: *
    
    # < cmd: <super user exec>
    elif (( ${#MSG_TEXT} > 0 )); then
        [ "$1" == "size" ] && ((MSG_SIZE+=36)) || \
            MSG_TEXT+="last \e[3;4mcmd/session\e[0m was running as root"
    fi
    
    # Return message text or size
    [ "$1" == "size" ] && echo $MSG_SIZE || echo -e "$MSG_TEXT"
}

# Print last exit code with description if signal and in different colors and background colors
function bashrc_get_exitcode() {
  local EXIT="$?"
  
  local SIGHUP="Hangup"
  local SIGINT="Terminal interrupt"
  local SIGQUIT="Terminal quit"
  local SIGILL="Illegal instruction"
  local SIGTRAP="Trace trap"
  local SIGABRT="Abort"
  local SIGBUS="BUS error"
  local SIGFPE="Floating point exception"
  local SIGKILL="Killed"
  local SIGUSR1="User defined signal 1"
  local SIGSEGV="Segmentation fault"
  local SIGUSR2="User defined signal 2"
  local SIGPIPE="Broken pipe"
  local SIGALRM="Alarm clock"
  local SIGTERM="Terminated"
  local SIGSTKFLT="Stack fault"
  local SIGCHLD="Child process stopped"
  local SIGCONT="Continue execution"
  local SIGSTOP="Stopped"
  local SIGTSTP="Terminal stop signal"
  local SIGTTIN="BG process trying to read TTY"
  local SIGTTOU="BG process trying to write TTY"
  local SIGURG="Urgent condition on socket"
  local SIGXCPU="CPU limit exceeded"
  local SIGXFSZ="File size limit exceeded"
  local SIGVTALRM="Virtual alarm clock"
  local SIGPROF="Profiling alarm clock"
  local SIGWINCH="Window size change"
  local SIGIO="I/O now possible"
  local SIGPWR="Power failure restart"
  
  local BASHSIG_EXEC="Bash: failed to execute command"
  local BASHSIG_CNF="Bash: command not found"
  local BASHSIG_EXIT="Bash: invalid argument to exit"
  
  if [ "$1" == "set" ]; then printf "%s" "$EXIT" > "$BASHRC_TMPFS/exitcode"
  elif [ "$1" == "description" ]; then
    EXIT=$(< "$BASHRC_TMPFS/exitcode")
    
    if   (( $EXIT == 126 )); then echo -e "\e[38;2;30;171;255m$BASHSIG_EXEC\e[0m";    # 126 BASH: exec error
    elif (( $EXIT == 127 )); then echo -e "\e[38;2;30;171;255m$BASHSIG_CNF\e[0m";     # 127 BASH: command not found
    elif (( $EXIT == 128 )); then echo -e "\e[38;2;30;171;255m$BASHSIG_EXIT\e[0m";    # 128 BAHS: invalid argument to exit
    
    # *NIX (POSIX, BSD, ANSI) Signals found on most common Unix-like operating systems
    elif (( $EXIT == 129 )); then echo -e "$SIGHUP";       # 129 n=1  SIGHUP  (hangup)
    elif (( $EXIT == 130 )); then echo -e "$SIGINT";       # 130 n=2  SIGINT  (interupt)
    elif (( $EXIT == 131 )); then echo -e "$SIGQUIT";      # 131 n=3  SIGQUIT (quit and core dump)
    elif (( $EXIT == 132 )); then echo -e "$SIGILL";       # 132 n=4  SIGILL  (illegal instruction)
    elif (( $EXIT == 133 )); then echo -e "$SIGTRAP";      # 133 n=5  SIGTRAP (trace/breakpoint trap)
    elif (( $EXIT == 134 )); then echo -e "$SIGABRT";      # 134 n=6  SIGABRT (abort)
    elif (( $EXIT == 135 )); then echo -e "$SIGBUS";       # 135 n=7  SIGBUS  (bus error)
    elif (( $EXIT == 136 )); then echo -e "$SIGFPE";       # 136 n=8  SIGFPE  (erroneous arithmetic operation)
    elif (( $EXIT == 137 )); then echo -e "$SIGKILL";      # 137 n=9  SIGKILL (killed)
    elif (( $EXIT == 138 )); then echo -e "$SIGUSR1";      # 138 n=10 SIGUSR1 (user defined signal 1)
    elif (( $EXIT == 139 )); then echo -e "$SIGSEGV";      # 139 n=11 SIGSEGV (segmentation fault)
    elif (( $EXIT == 140 )); then echo -e "$SIGUSR2";      # 140 n=12 SIGUSR2 (user defined signal 2)
    elif (( $EXIT == 141 )); then echo -e "$SIGPIPE";      # 141 n=13 SIGPIPE (broken pipe)
    elif (( $EXIT == 142 )); then echo -e "$SIGALRM";      # 142 n=14 SIGALRM (alarm clock)
    elif (( $EXIT == 143 )); then echo -e "$SIGTERM";      # 143 n=15 SIGTERM (termination)
    elif (( $EXIT == 144 )); then echo -e "$SIGSTKFLT";    # 144 n=16 SIGSTKFLT (stack fault)
    elif (( $EXIT == 145 )); then echo -e "$SIGCHLD";      # 145 n=17 SIGCHLD (child stopped or exited)
    elif (( $EXIT == 146 )); then echo -e "$SIGCONT";      # 146 n=18 SIGCONT (continue execution SIGSTOP)
    elif (( $EXIT == 147 )); then echo -e "$SIGSTOP";      # 147 n=19 SIGSTOP (stop execution)
    elif (( $EXIT == 148 )); then echo -e "$SIGTSTP";      # 148 n=20 SIGTSTP (terminal stop)
    elif (( $EXIT == 149 )); then echo -e "$SIGTTIN";      # 149 n=21 SIGTTIN (bg process trying to read tty)
    elif (( $EXIT == 150 )); then echo -e "$SIGTTOU";      # 150 n=22 SIGTTOU (bg process trying to write tty)
    elif (( $EXIT == 151 )); then echo -e "$SIGURG";       # 151 n=23 SIGURG  (urgent condition on socket)
    elif (( $EXIT == 152 )); then echo -e "$SIGXCPU";      # 152 n=24 SIGXCPU (cpu limit exceeded)
    elif (( $EXIT == 153 )); then echo -e "$SIGXFSZ";      # 153 n=25 SIGXFSZ (file size limit exceeded)
    elif (( $EXIT == 154 )); then echo -e "$SIGVTALRM"     # 154 n=26 SIGVTALRM (virtual timer expired)
    elif (( $EXIT == 155 )); then echo -e "$SIGPROF"       # 155 n=27 SIGPROF (profiling timer expired)
    elif (( $EXIT == 156 )); then echo -e "$SIGWINCH";     # 156 n=28 SIGWINCH (window size change)
    elif (( $EXIT == 157 )); then echo -e "$SIGIO";        # 157 n=29 SIGIO   (I/O now possible)
    elif (( $EXIT == 158 )); then echo -e "$SIGPWR";       # 158 n=30 SIGPWR  (power failure)
    else bashrc_extended_description_util;
    fi
  elif [ "$1" == "description-size" ]; then
    EXIT=$(< "$BASHRC_TMPFS/exitcode")
    
    if   (( $EXIT == 126 )); then echo "${#BASHSIG_EXEC}"; # 126 BASH: exec error
    elif (( $EXIT == 127 )); then echo "${#BASHSIG_CNF}";  # 127 BASH: command not found
    elif (( $EXIT == 128 )); then echo "${#BASHSIG_EXIT}"; # 128 BAHS: invalid argument to exit
    
    # *NIX (POSIX, BSD, ANSI) Signals found on most common Unix-like operating systems
    elif (( $EXIT == 129 )); then echo "${#SIGHUP}";       # 129 n=1  SIGHUP  (hangup)
    elif (( $EXIT == 130 )); then echo "${#SIGINT}";       # 130 n=2  SIGINT  (interupt)
    elif (( $EXIT == 131 )); then echo "${#SIGQUIT}";      # 131 n=3  SIGQUIT (quit and core dump)
    elif (( $EXIT == 132 )); then echo "${#SIGILL}";       # 132 n=4  SIGILL  (illegal instruction)
    elif (( $EXIT == 133 )); then echo "${#SIGTRAP}";      # 133 n=5  SIGTRAP (trace/breakpoint trap)
    elif (( $EXIT == 134 )); then echo "${#SIGABRT}";      # 134 n=6  SIGABRT (abort)
    elif (( $EXIT == 135 )); then echo "${#SIGBUS}";       # 135 n=7  SIGBUS  (bus error)
    elif (( $EXIT == 136 )); then echo "${#SIGFPE}";       # 136 n=8  SIGFPE  (erroneous arithmetic operation)
    elif (( $EXIT == 137 )); then echo "${#SIGKILL}";      # 137 n=9  SIGKILL (killed)
    elif (( $EXIT == 138 )); then echo "${#SIGUSR1}";      # 138 n=10 SIGUSR1 (user defined signal 1)
    elif (( $EXIT == 139 )); then echo "${#SIGSEGV}";      # 139 n=11 SIGSEGV (segmentation fault)
    elif (( $EXIT == 140 )); then echo "${#SIGUSR2}";      # 140 n=12 SIGUSR2 (user defined signal 2)
    elif (( $EXIT == 141 )); then echo "${#SIGPIPE}";      # 141 n=13 SIGPIPE (broken pipe)
    elif (( $EXIT == 142 )); then echo "${#SIGALRM}";      # 142 n=14 SIGALRM (alarm clock)
    elif (( $EXIT == 143 )); then echo "${#SIGTERM}";      # 143 n=15 SIGTERM (termination)
    elif (( $EXIT == 144 )); then echo "${#SIGSTKFLT}";    # 144 n=16 SIGSTKFLT (stack fault)
    elif (( $EXIT == 145 )); then echo "${#SIGCHLD}";      # 145 n=17 SIGCHLD (child stopped or exited)
    elif (( $EXIT == 146 )); then echo "${#SIGCONT}";      # 146 n=18 SIGCONT (continue execution SIGSTOP)
    elif (( $EXIT == 147 )); then echo "${#SIGSTOP}";      # 147 n=19 SIGSTOP (stop execution)
    elif (( $EXIT == 148 )); then echo "${#SIGTSTP}";      # 148 n=20 SIGTSTP (terminal stop)
    elif (( $EXIT == 149 )); then echo "${#SIGTTIN}";      # 149 n=21 SIGTTIN (bg process trying to read tty)
    elif (( $EXIT == 150 )); then echo "${#SIGTTOU}";      # 150 n=22 SIGTTOU (bg process trying to write tty)
    elif (( $EXIT == 151 )); then echo "${#SIGURG}";       # 151 n=23 SIGURG  (urgent condition on socket)
    elif (( $EXIT == 152 )); then echo "${#SIGXCPU}";      # 152 n=24 SIGXCPU (cpu limit exceeded)
    elif (( $EXIT == 153 )); then echo "${#SIGXFSZ}";      # 153 n=25 SIGXFSZ (file size limit exceeded)
    elif (( $EXIT == 154 )); then echo "${#SIGVTALRM}";    # 154 n=26 SIGVTALRM (virtual timer expired)
    elif (( $EXIT == 155 )); then echo "${#SIGPROF}";      # 155 n=27 SIGPROF (profiling timer expired)
    elif (( $EXIT == 156 )); then echo "${#SIGWINCH}";     # 156 n=28 SIGWINCH (window size change)
    elif (( $EXIT == 157 )); then echo "${#SIGIO}";        # 157 n=29 SIGIO   (I/O now possible)
    elif (( $EXIT == 158 )); then echo "${#SIGPWR}";       # 158 n=30 SIGPWR  (power failure)
    else bashrc_extended_description_util size;
    fi
  elif [ "$1" == "strlen" ]; then
    EXIT=$(< "$BASHRC_TMPFS/exitcode")
    
    # 1: Catchall for general errors
    # 2: Misuse of shell builtins (according to Bash documentation)
    # 126: Command invoked cannot execute
    # 127: "command not found"
    # 128: Invalid argument to exit
    # 128+n: Fatal error signal "n"
    # 255: Exit status out of range (exit takes only integer args in the range 0 - 255)
    
    if   (( $EXIT == 0   )); then       echo ${#EXIT};
    elif (( $EXIT == 126 )); then       echo 4;
    elif (( $EXIT == 127 )); then       echo 3;
    elif (( $EXIT == 128 )); then       echo 4;
    
    # *NIX (POSIX, BSD, ANSI) Signals found on most common Unix-like operating systems
    elif (( $EXIT == 129 )); then       echo 6;       # 129 n=1  SIGHUP  (hangup)
    elif (( $EXIT == 130 )); then       echo 6;       # 130 n=2  SIGINT  (interupt)
    elif (( $EXIT == 131 )); then       echo 7;       # 131 n=3  SIGQUIT (quit and core dump)
    elif (( $EXIT == 132 )); then       echo 6;       # 132 n=4  SIGILL  (illegal instruction)
    elif (( $EXIT == 133 )); then       echo 7;       # 133 n=5  SIGTRAP (trace/breakpoint trap)
    elif (( $EXIT == 134 )); then       echo 7;       # 134 n=6  SIGABRT (abort)
    elif (( $EXIT == 135 )); then       echo 6;       # 135 n=7  SIGBUS  (bus error)
    elif (( $EXIT == 136 )); then       echo 6;       # 136 n=8  SIGFPE  (erroneous arithmetic operation)
    elif (( $EXIT == 137 )); then       echo 7;       # 137 n=9  SIGKILL (killed)
    elif (( $EXIT == 138 )); then       echo 7;       # 138 n=10 SIGUSR1 (user defined signal 1)
    elif (( $EXIT == 139 )); then       echo 7;       # 139 n=11 SIGSEGV (segmentation fault)
    elif (( $EXIT == 140 )); then       echo 7;       # 140 n=12 SIGUSR2 (user defined signal 2)
    elif (( $EXIT == 141 )); then       echo 7;       # 141 n=13 SIGPIPE (broken pipe)
    elif (( $EXIT == 142 )); then       echo 7;       # 142 n=14 SIGALRM (alarm clock)
    elif (( $EXIT == 143 )); then       echo 7;       # 143 n=15 SIGTERM (termination)
    elif (( $EXIT == 144 )); then       echo 9;       # 144 n=16 SIGSTKFLT (stack fault)
    elif (( $EXIT == 145 )); then       echo 7;       # 145 n=17 SIGCHLD (child stopped or exited)
    elif (( $EXIT == 146 )); then       echo 7;       # 146 n=18 SIGCONT (continue execution SIGSTOP)
    elif (( $EXIT == 147 )); then       echo 7;       # 147 n=19 SIGSTOP (stop execution)
    elif (( $EXIT == 148 )); then       echo 7;       # 148 n=20 SIGTSTP (terminal stop)
    elif (( $EXIT == 149 )); then       echo 7;       # 149 n=21 SIGTTIN (bg process trying to read tty)
    elif (( $EXIT == 150 )); then       echo 7;       # 150 n=22 SIGTTOU (bg process trying to write tty)
    elif (( $EXIT == 151 )); then       echo 6;       # 151 n=23 SIGURG  (urgent condition on socket)
    elif (( $EXIT == 152 )); then       echo 7;       # 152 n=24 SIGXCPU (cpu limit exceeded)
    elif (( $EXIT == 153 )); then       echo 7;       # 153 n=25 SIGXFSZ (file size limit exceeded)
    elif (( $EXIT == 154 )); then       echo 9;       # 154 n=26 SIGVTALRM (virtual timer expired)
    elif (( $EXIT == 155 )); then       echo 7;       # 155 n=27 SIGPROF (profiling timer expired)
    elif (( $EXIT == 156 )); then       echo 8;       # 156 n=28 SIGWINCH (window size change)
    elif (( $EXIT == 157 )); then       echo 5;       # 157 n=29 SIGIO   (I/O now possible)
    elif (( $EXIT == 158 )); then       echo 6;       # 158 n=30 SIGPWR  (power failure)
    
    elif (( $EXIT >  158 && $EXIT < 255 )); then   echo ${#EXIT}
    elif (( $EXIT == 255 )); then                  echo ${#EXIT}
    else                                           echo ${#EXIT}
    fi
  else
    EXIT=$(< "$BASHRC_TMPFS/exitcode")
    
    # 1: Catchall for general errors
    # 2: Misuse of shell builtins (according to Bash documentation)
    # 126: Command invoked cannot execute
    # 127: "command not found"
    # 128: Invalid argument to exit
    # 128+n: Fatal error signal "n"
    # 255: Exit status out of range (exit takes only integer args in the range 0 - 255)
    
    if   (( $EXIT == 0   )); then                  echo            "$EXIT";            # 0        black -> success
    elif (( $EXIT >  0   && $EXIT < 126 )); then   echo -e "\e[0;31m$EXIT\e[0m";       # 1~125    red -> error
    elif (( $EXIT == 126 )); then   echo -e "\e[48;2;30;171;255m\e[38;2;255;255;255mEXEC\e[0m";  # 126      blue -> EXEC error
    elif (( $EXIT == 127 )); then   echo -e "\e[48;2;30;171;255m\e[38;2;255;255;255mCNF\e[0m";   # 127      blue -> CNF error
    elif (( $EXIT == 128 )); then   echo -e "\e[48;2;30;171;255m\e[38;2;255;255;255mEXIT\e[0m";  # 128      blue -> EXIT error
    
    # *NIX (POSIX, BSD, ANSI) Signals found on most common Unix-like operating systems
    elif (( $EXIT == 129 )); then       echo -e "\e[0;35mSIGHUP\e[0m";       # 129 n=1  SIGHUP  (hangup)
    elif (( $EXIT == 130 )); then echo -e "\e[0;45m\e[97mSIGINT\e[0m";       # 130 n=2  SIGINT  (interupt)
    elif (( $EXIT == 131 )); then       echo -e "\e[0;35mSIGQUIT\e[0m";      # 131 n=3  SIGQUIT (quit and core dump)
    elif (( $EXIT == 132 )); then echo -e "\e[0;41m\e[97mSIGILL\e[0m";       # 132 n=4  SIGILL  (illegal instruction)
    elif (( $EXIT == 133 )); then       echo -e "\e[0;35mSIGTRAP\e[0m";      # 133 n=5  SIGTRAP (trace/breakpoint trap)
    elif (( $EXIT == 134 )); then       echo -e "\e[0;35mSIGABRT\e[0m";      # 134 n=6  SIGABRT (abort)
    elif (( $EXIT == 135 )); then       echo -e "\e[0;35mSIGBUS\e[0m";       # 135 n=7  SIGBUS  (bus error)
    elif (( $EXIT == 136 )); then echo -e "\e[0;41m\e[97mSIGFPE\e[0m";       # 136 n=8  SIGFPE  (erroneous arithmetic operation)
    elif (( $EXIT == 137 )); then echo -e "\e[0;45m\e[97mSIGKILL\e[0m";      # 137 n=9  SIGKILL (killed)
    elif (( $EXIT == 138 )); then echo -e "\e[0;46m\e[97mSIGUSR1\e[0m";      # 138 n=10 SIGUSR1 (user defined signal 1)
    elif (( $EXIT == 139 )); then echo -e "\e[0;41m\e[97mSIGSEGV\e[0m";      # 139 n=11 SIGSEGV (segmentation fault)
    elif (( $EXIT == 140 )); then echo -e "\e[0;46m\e[97mSIGUSR2\e[0m";      # 140 n=12 SIGUSR2 (user defined signal 2)
    elif (( $EXIT == 141 )); then       echo -e "\e[0;35mSIGPIPE\e[0m";      # 141 n=13 SIGPIPE (broken pipe)
    elif (( $EXIT == 142 )); then       echo -e "\e[0;35mSIGALRM\e[0m";      # 142 n=14 SIGALRM (alarm clock)
    elif (( $EXIT == 143 )); then echo -e "\e[0;45m\e[97mSIGTERM\e[0m";      # 143 n=15 SIGTERM (termination)
    elif (( $EXIT == 144 )); then echo -e "\e[0;41m\e[97mSIGSTKFLT\e[0m";    # 144 n=16 SIGSTKFLT (stack fault)
    elif (( $EXIT == 145 )); then       echo -e "\e[0;35mSIGCHLD\e[0m";      # 145 n=17 SIGCHLD (child stopped or exited)
    elif (( $EXIT == 146 )); then echo -e "\e[0;44m\e[97mSIGCONT\e[0m";      # 146 n=18 SIGCONT (continue execution SIGSTOP)
    elif (( $EXIT == 147 )); then echo -e "\e[0;44m\e[97mSIGSTOP\e[0m";      # 147 n=19 SIGSTOP (stop execution)
    elif (( $EXIT == 148 )); then echo -e "\e[0;44m\e[97mSIGTSTP\e[0m";      # 148 n=20 SIGTSTP (terminal stop)
    elif (( $EXIT == 149 )); then echo -e "\e[0;44m\e[97mSIGTTIN\e[0m";      # 149 n=21 SIGTTIN (bg process trying to read tty)
    elif (( $EXIT == 150 )); then echo -e "\e[0;44m\e[97mSIGTTOU\e[0m";      # 150 n=22 SIGTTOU (bg process trying to write tty)
    elif (( $EXIT == 151 )); then       echo -e "\e[0;35mSIGURG\e[0m";       # 151 n=23 SIGURG  (urgent condition on socket)
    elif (( $EXIT == 152 )); then echo -e "\e[0;43m\e[97mSIGXCPU\e[0m";      # 152 n=24 SIGXCPU (cpu limit exceeded)
    elif (( $EXIT == 153 )); then echo -e "\e[0;43m\e[97mSIGXFSZ\e[0m";      # 153 n=25 SIGXFSZ (file size limit exceeded)
    elif (( $EXIT == 154 )); then echo -e "\e[0;43m\e[97mSIGVTALRM\e[0m"     # 154 n=26 SIGVTALRM (virtual timer expired)
    elif (( $EXIT == 155 )); then echo -e "\e[0;43m\e[97mSIGPROF\e[0m"       # 155 n=27 SIGPROF (profiling timer expired)
    elif (( $EXIT == 156 )); then       echo -e "\e[0;35mSIGWINCH\e[0m";     # 156 n=28 SIGWINCH (window size change)
    elif (( $EXIT == 157 )); then echo -e "\e[0;40m\e[97mSIGIO\e[0m";        # 157 n=29 SIGIO   (I/O now possible)
    elif (( $EXIT == 158 )); then echo -e "\e[1;41m\e[97mSIGPWR\e[0m"        # 158 n=30 SIGPWR  (power failure)
    
    elif (( $EXIT >  158 && $EXIT < 255 )); then   echo -e "\e[0;31m$EXIT\e[0m"; # 129~254  red -> error
    elif (( $EXIT == 255 )); then                  echo -e "\e[1;31m$EXIT\e[0m"; # 255      red (bold) -> exit status exceed
    else                                           echo            "$EXIT";      # *        black [just in case]
    fi
  fi
}

# Resets the last custom message after 3 seconds, uses ANSI/VT100 Terminal ESC sequences
#  <ESC>[s  Save Cursor Position
#  <ESC>[u  Restore Cursor Position
#  <ESC>[1A Move Cursor 1 line UP
#  \r       Move Cursor to begin of line
#  <ESC>[1B Move Cursor 1 line DOWN
#
# Usage: $0 <line start = $BASHRC_LINESTARTER> <len of msg>
# TODO: × implement <len of msg>
#       × find a way to integrate this piece of code into the prompt
#           --> bashrc_reset_statusmessage "$BASHRC_LINESTARTER" &
#         works, but messes around with my job monitor, since this is just a normal background job
#       × add compatibility with my git status monitor
#
# NOTE: I wish bash would use the ncurses library to allow dynamic and realtime updating prompts =(
#
function bashrc_reset_statusmessage() {
    sleep 3
    echo -en "\e[s\e[1A\r$1                                          \e[1B\e[u"
}

# Get current system time in format HH:MM:SS
function bashrc_get_currenttime() {
  date +"%T"
}

# uname -s -r [-p] in that order
#  -s Operating System
#  -r Kernel Release (stripped, show everything before '-')
#  -m Processor Type (Architecture)
function bashrc_print_osdetails() {
    if [ "$1" == "strsize" ]; then
        local OSDETAILS_STRING="$(uname -s) $(uname -r | sed 's/-.*//g') [$(uname -m)]"
        echo ${#OSDETAILS_STRING} # return string length
    else
        echo -e "\[\e[38;5;240m\e[1m\]$(uname -s)\[\e[0m\] \[\e[38;5;243m\e[3m\]$(uname -r | sed 's/-.*//g')\[\e[0m\] \[\e[38;5;250m\][$(uname -m)]\[\e[0m\]" # return formatted string (only for $PS1)
    fi
}

# Get jobs currently running and stopped ─ contains length of string for separator function
function bashrc_get_jobscount() {
   local stopped="$(jobs -s | wc -l | tr -d " ")"
   local running="$(jobs -r | wc -l | tr -d " ")"
   local jobs_string="${running}r/${stopped}s"
   local jobs_strsize="${#jobs_string}"
   if [ "$1" == "strsize" ]; then echo "$jobs_strsize"
   else echo "$jobs_string"; fi
}

fi # --- PS1~4

# Inserts a separator of $1, optimal: substrate $2 number to fill the rest of the line
function bashrc_insert_separator() {
  # define local variables
  local fillsize=""
  local fill=""
  # check if $1 exists, if not set it to space
  local fillchar=${1:-" "}
  # check if $2 exists, if not set it to 0
  local minusfill=${2:-"0"}
  # calculate fillsize
  fillsize=$(($COLUMNS - $minusfill))
  # insert separator
  if [ "$fillchar" == " " ]; then printf '%*s\n' "${fillsize}" ''
  else
    while [ "$fillsize" -gt "0" ]; do
        fill="$fillchar${fill}"
        ((fillsize--))
    done
    # print separator
    echo $fill
  fi
}

if [ ! -z "$PS1" ]; then # --- PS1~4

# This function prints the current working directory on screen, but with some little extras
#  -- respect terminal columns and enters a newline instead of the ugly line wrapping
#  -- don't trim in middle of file or folder name if no space left
#     makes it easier to see the directory tree
#     if for some reason the file/folder name is longer than the terminal columns, the name is wrapped into a new line
#  -- perfect for fancy dynamic bash prompts
#
#  Usage: $0 [int-extra-indentation-for-firstline] [int-extra-indentation-for-lines-except-firstline]
#
function bashrc_pwd_resolve_homepath() { [[ "$PWD" =~ ^"$HOME"(/|$) ]] && echo "~${PWD#$HOME}" || echo "$PWD"; }
function bashrc_pretty_print_pwd() {
    if [ "$1" == "pwdlen" ]; then
        echo "$(< "$BASHRC_TMPFS/pwdlen")"
        return 0
    fi
    
    # every possible new line starts with this (except the first one)
    local newline_starter="# │ " # TODO: $2
    
    # every possible line ends with this (including first line)
    local newline_ender=" │"

    # calculate available space
    local columns_firstline=$(($COLUMNS - $1))
    local columns_other=$(($COLUMNS - ${#newline_starter} - ${#newline_ender}))
    local columns_tmp # buffer, for each line
    local buf_firstline # indicator for first line
    
    # store 'pwd' into an array, split at directory indicator '/'
    local pwd="$(bashrc_pwd_resolve_homepath)"
    IFS='/' read -r -a pwd <<< "$pwd"
    
    # remove empty elements, example: `cd /bin` would result in "//bin" without this
    for i in "${!pwd[@]}"; do
        [ -n "${pwd[$i]}" ] || unset "pwd[$i]"
    done
    
    # create output buffer (must be plain ─ no escape sequences)
    local outbuf
    local formatbuf # formated string (with escape sequences; to avoid extra commands, like sed ─ for best performance)
    
    local dirindicator="\e[1;38;2;79;37;74m/\e[0m"
    local dirindicator_len="${#dirindicator}"
    
    # append first '/' if not starts with tilde~
    if [ "${pwd[0]}" != "~" ]; then outbuf+="/"; formatbuf+="$dirindicator"; fi
    
    # TODO <more or less everything>
    # 
    # CHANGELOG:
    #  × improved performance and fixed unicode wide character (aka 2 columns per char) bug
    #  × moved around some code to make it more readable, removed some junk for performance improvement
    #
    # ALREADY IMPLEMENTED AS SOME KIND OF WORKAROUND FOR NOW:
    #  × If less than 100 columns:
    #    Trim filename at 13th character and replace with wildcard (to shorten the directory tree)
    #    PRO: path can be still copy-pasted and used, because of the wildcard :)
    #    CON: path may not be correct... (need to check with 'pwd')
    #
    
    # We need different column count for the firstline
    #buf_firstline="true"
    
    # filename length buffers
    #local pwd_fn_length
    #local pwd_fn_length_helper
    
    for i in "${pwd[@]}"; do
        if (( $COLUMNS < 100 )); then
            if (( ${#i} > 12 )); then
                i="${i:0:12}*"
            fi
        fi
        
        # append to output buffer
        outbuf+="${i}/"
        
        ### ~~~ Set the temporary column buffer
        #[ "$buf_firstline" == "true" ] && columns_tmp=$columns_firstline || columns_tmp=$columns_other
        #local columns_helper=$columns_tmp
        
        ### ~~~ Calculate the current filename length
        #pwd_fn_length=${#i}
        
        #if (( $pwd_fn_length > $columns_tmp )); then
        #    outbuf+="${i:0:columns_tmp}$newline_ender\n"
        #    outbuf+="$newline_starter${i:columns_tmp}"
        #    pwd_fn_length_helper="${i:columns_tmp}"
        #    pwd_fn_length_helper=${#pwd_fn_length_helper}
        #    ((columns_tmp-=$pwd_fn_length_helper))
        #    buf_firstline="false"
        #else
        #    if (( $columns_tmp > $pwd_fn_length )); then
        #        outbuf+="${i}"
        #        ((columns_tmp-=$pwd_fn_length))
        #    else
        #        ((columns_tmp-=$pwd_fn_length))
        #        outbuf+="$(printf '%*s\n' "$(($columns_tmp-1))" '')$newline_ender\n$newline_starter"
        #        outbuf+="${i}"
        #        pwd_fn_length_helper="${i:columns_tmp}"
        #        pwd_fn_length_helper=${#pwd_fn_length_helper}
        #        columns_tmp=$(($columns_helper - $pwd_fn_length_helper))
        #        buf_firstline="false"
        #    fi
        #fi
        
        # display symlinks in different color
        [[ ! -z $(readlink "${outbuf::-1}") ]] && formatbuf+="\e[38;2;200;0;200m${i}\e[0m${dirindicator}" \
                                               || formatbuf+="${i}${dirindicator}"
    done
    
    ### ~~~ finalize string
    ###  × colorize the $HOME tilde~ in $formatbuf
    ###  × save the path length for the separator function (unicode friendly, alias chars which require 2 columns)
    
    # cut operations
    if (( ${#outbuf} != 1 )); then outbuf="${outbuf::-1}"; formatbuf="${formatbuf::-${dirindicator_len}}"; fi
    
    # most unicode chars require 2 columns instead of just the regular 1, this tiny and hyperfast utility calculates the length
    printf "%s" "$("/usr/local/share/bashrc-tools/strcolumns" "${outbuf}")" > "$BASHRC_TMPFS/pwdlen"
    
    # format the tilde~
    [ "${formatbuf:0:1}" == "~" ] && formatbuf="\e[90m~\e[0m${formatbuf:1}"
    
    # return the path
    echo -e -n "$formatbuf"
}

function bashrc_get_pwd_inodes() {
    if [ "$1" == "strlen" ]; then
        local STRLEN=$(ls | wc -l | tr -d '[:blank:]') # tr is required on Mac OSX, because of 'wc' stupidity ;(
        echo ${#STRLEN}
    else
        echo $(ls | wc -l | tr -d '[:blank:]') # tr is required on Mac OSX, because of 'wc' stupidity ;(
    fi
}

# This function resets the terminal to its defaults
# 
# Whenever you face a buggy developed program or shell script,
# which forgets to reset font formattings and colors, your prompt
# can become very VERY ugly. To avoid this, this function resets
# the terminal whenever the prompt is reached. $PS1
function bashrc_reset_terminal() {
    # OLD CODE -> slow and NOT efficient because we spawn two processes in a row... [ALSO NOT PORTABLE]
    #tput sgr0     # Restore color profile and font settings
    #tput cnorm    # Restore cursor, if it was invisible before
    
    printf '\033[0m\033[?25h' # Same as above, but using ANSI escape sequences and builtin printf command ─ faster!
}

### ~~~ GIT Status Monitor ~~~
function bashrc_git_status_monitor() {

    # Check if 'pwd' is a git repository
    git status > /dev/null 2>&1 || return 1
    
    # read the git repository
    local git_status="# \e[38;2;0;217;174m(git)\e[0m\e[1m$\e[0m "
    local git_data=($(python2 '/build/bash-git-prompt/gitstatus.py') )
    
    # == git_data array data
    # branch name: [0]
    # remote branch: [1]
    # staged: [2]
    # conflicts: [3]
    # changed: [4]
    # untracked: [5]
    # stashed: [6]
    # clean: [7]
    
    git_status+="〔\e[35m${git_data[0]}\e[0m|"
    git_status+="\e[38;2;85;85;255m${git_data[1]}\e[0m〕 "
    git_status+="\e[38;2;0;255;0m● \e[0m${git_data[2]}, "
    git_status+="\e[38;2;255;0;0m✖ \e[0m${git_data[3]}, "
    git_status+="\e[38;2;184;184;91m✚ \e[0m${git_data[4]}, "
    git_status+="\e[38;2;124;124;184m… \e[0m${git_data[5]}, "
    git_status+="\e[38;2;77;155;232m⚑ \e[0m${git_data[6]} | "
    
    if (( ${git_data[7]} == 1 )); then
        git_status+="\e[1m\e[38;2;46;145;0mclean\e[0m \e[38;2;46;145;0m☑ \e[0m"
    else
    
        # first check for merge conflicts
        if (( ${git_data[3]} != 0 )); then
            git_status+="\e[1m\e[38;2;255;0;0mMERGE CONFLICTS!!\e[0m"
        
        else
        
            # check for changed files
            if (( ${git_data[4]} != 0 )); then
                git_status+="\e[1m\e[38;2;184;184;91mchanged files\e[0m, "
            fi
            
            # check for stashed files
            if (( ${git_data[6]} != 0 )); then
                git_status+="\e[1m\e[38;2;77;155;232mstashed files\e[0m, "
            fi
            
            # check for untracked files
            if (( ${git_data[5]} != 0 )); then
                git_status+="\e[1m\e[38;2;124;124;184muntracked files\e[0m, "
            fi
            
            # check for staged files
            if (( ${git_data[2]} != 0 )); then
                git_status+="\e[1m\e[38;2;112;110;127mout of date\e[0m, "
            fi
            
            # CLEAN UP STRING
            [[ "$git_status" == *", " ]] && git_status="${git_status::-2}"
        
        fi
    
    fi
    
    # done reading the git repository, return the results
    echo -e "$git_status"
    return 0
}

### ~~~ SVN Status Monitor ~~~
function bashrc_svn_status_monitor() {

    # Check if 'pwd' is a git repository
    svn info > /dev/null 2>&1 || return 1
    
    # read the svn repository
    local svn_status="# \e[38;2;0;217;174m(svn)\e[0m\e[1m$\e[0m "
    
    local svn_relurl=$(svn info --show-item relative-url | cut -f2 -d/)
    local svn_revision=$(svn info --show-item revision | tr -d ' ')
    local svn_timestapmp=$(TZ=UTC date -d"$(svn info --show-item last-changed-date)" +"%Y-%m-%d %T UTC")
    
    svn_status+="〔\e[35m${svn_relurl}\e[0m|"
    svn_status+="\e[38;2;85;85;255m${svn_revision}\e[0m〕 "
    svn_status+="\e[38;2;91;91;91mT[${svn_timestapmp}]\e[0m"
    
    echo -e "$svn_status"
    return 0
}

##################################################################################
# PS1 ─ User Input                                                               #
##################################################################################
# NOTE: I'm NOT using any of these Bash internal escape sequences for hostname, etc...
#       Bash's line counting --> \[ escape sequence \] characters are very important
#       to prevent issues in the prompt
#       Bash is counting the amount of chars in $PS1 to calculate line wrapping or something...

fi # --- PS1~4

# need this several times, store to prevent useless process creation...
BASHRC_WHOAMI="$(whoami)"
BASHRC_HOSTNAME="$(hostname)"

# executed only once per session ─ this code greps the full username from the /etc/passwd file
# -- workaround for a 'all-lowercase-username', Redhat-based distros doesn't have this issue
# -- personally I prefer a free usernaming like Redhat-based distros does
PASSWD_CURRENT_USER_FULLNAME="$(awk -F":" '{ print $1$5 }' /etc/passwd | grep "$BASHRC_WHOAMI" | sed "s/^$BASHRC_WHOAMI//" | tr -d ',')"

if [ ! -z "$PS1" ]; then # --- PS1~4

# executed only once per session ─ this code generates the header line (along with its size for the separator function),
#                                  aswell as the input line (the root user has a different one)
if [[ $EUID -eq 0 ]]; then # (root) is in red
BASHRC_USERHOSTNAME="$(echo -e "\[\e[1m\e[38;2;177;50;28m\]$BASHRC_HOSTNAME\[\e[0m\]")"
BASHRC_USERHOSTNAME_SIZE="$BASHRC_HOSTNAME"
BASHRC_USERHOSTNAME_SIZE=${#BASHRC_USERHOSTNAME_SIZE}
BASHRC_LINESTARTER="$(echo -e "\[\e[38;2;230;44;44m\]#\[\e[0m\]")"
PS1_CMDINPUT="$BASHRC_LINESTARTER ($(echo -e "\[\e[38;2;230;44;44m\]root\[\e[0m\]"))\$ "
else # (other users) are in the default color
BASHRC_USERHOSTNAME="$PASSWD_CURRENT_USER_FULLNAME@$(echo -e "\[\e[1m\e[38;2;177;50;28m\]$BASHRC_HOSTNAME\[\e[0m\]")"
BASHRC_USERHOSTNAME_SIZE="$PASSWD_CURRENT_USER_FULLNAME@$BASHRC_HOSTNAME"
BASHRC_USERHOSTNAME_SIZE=${#BASHRC_USERHOSTNAME_SIZE}
BASHRC_LINESTARTER="#"
PS1_CMDINPUT="$BASHRC_LINESTARTER > "
fi
BASHRC_LOCALTIME_SIZE="$(bashrc_get_currenttime)"
BASHRC_LOCALTIME_SIZE=${#BASHRC_LOCALTIME_SIZE}
BASHRC_OSDETAILS="$(bashrc_print_osdetails)"
PS1_HEADER_SIZE=$(($BASHRC_USERHOSTNAME_SIZE + $BASHRC_LOCALTIME_SIZE + $(bashrc_print_osdetails strsize) + 22))
unset BASHRC_USERHOSTNAME_SIZE
unset BASHRC_LOCALTIME_SIZE

BASHRC_TERMINAL="$(tty | sed 's/^\/dev\///')"
BASHRC_TERMINAL_SIZE=${#BASHRC_TERMINAL}

# executed each time when a command finished
PS1_HEADER="$BASHRC_LINESTARTER───[ ${BASHRC_USERHOSTNAME} ]─\$(bashrc_insert_separator ─ $PS1_HEADER_SIZE)─( ${BASHRC_OSDETAILS} )────[\[$(echo -e "\e[1m\e[38;2;62;62;62m")\]\$(bashrc_get_currenttime)\[$(echo -e "\e[0m")\]]─┐\n"
PS1_MAINLINE="$BASHRC_LINESTARTER [\$(bashrc_get_exitcode)] CWD[\$(bashrc_get_pwd_inodes)]: \$(bashrc_pretty_print_pwd \$((\$(bashrc_get_exitcode strlen) + \$(bashrc_get_pwd_inodes strlen) + 12))) \$(bashrc_insert_separator \" \" \$((\$(bashrc_get_exitcode strlen)+\$(bashrc_get_pwd_inodes strlen)+\$(bashrc_pretty_print_pwd pwdlen)+15))) │\n"
PS1_INFOLINE="$BASHRC_LINESTARTER \$(bashrc_get_exitcode description) \$(bashrc_insert_separator \" \" \$((\$(bashrc_get_jobscount strsize) + $BASHRC_TERMINAL_SIZE + 27 + \$(bashrc_get_exitcode description-size) - 10))) [\[$(echo -e "\e[1m\e[38;2;70;130;180m")\]Jobs:\[$(echo -e "\e[0m")\] \$(bashrc_get_jobscount)]─[${BASHRC_TERMINAL}]─┘\n"
PS1_GITMONITOR="\$(if bashrc_git_status_monitor; then echo -e '\n '; fi)"
PS1_SVNMONITOR="\$(if bashrc_svn_status_monitor; then echo -e '\n '; fi)"
#NEWLINEWRAPPER="\[\$(echo $'\n')\]"

# PS1 explanation
#  × `bashrc_get_exitcode set` saves the exit status of the last running command into a temporary file,
#    because the $? variable is overridden several times, but I need it for several tweaks across the prompt
#  × `bashrc_reset_terminal` resets the terminal to the default state; for buggy applications
#  × ${PS1_HEADER}: contains username@hostname, separator line, OS and the current time (not dynamically updated)
#  × ${PS1_MAINLINE}: contains last exit code, visible inodes of current working directory and the current working directory
#  × ${PS1_INFOLINE}: contains explanation of last exit code (from internal database) a job monitor and the terminal type/number
#  × ${PS1_GITMONITOR}: git monitor, activates when cd'ing into a git repository
#  × ${PS1_CMDINPUT}: is the actual input line of the prompt, because its in a new line, you have maximum space :)
PS1="\[\$(bashrc_get_exitcode set)\$(bashrc_reset_terminal)\]\n${PS1_HEADER}${PS1_MAINLINE}${PS1_INFOLINE}${PS1_GITMONITOR}${PS1_SVNMONITOR}${PS1_CMDINPUT}"

##################################################################################
# PS2 ─ Read more User Input (multiline)                                     (\) #
##################################################################################
PS2="# ┊ "

##################################################################################
# PS3 ─ select prompt     [in case there is no one set internally in the script] #
##################################################################################
export PS3="# $(printf '\e[1m')[select]$(printf '\e[0m') > "

##################################################################################
# PS4 ─ Script Debugging                                                (xtrace) #
##################################################################################
# # $script_name@line[$LINENO] > $function(): $line_content
export PS4="# \e[38;5;239m(xtrace)\e[0m \e[4m\e[90m\$(basename \"\${BASH_SOURCE}\")\e[39m@\e[3mline\e[1m[\${LINENO}]\e[24m \e[1;34m>\e[0m \e[38;5;105m\${FUNCNAME[0]}()\e[0m: "

##################################################################################
# Info Line                [on top of every terminal, useful when saving output] #
##################################################################################
# NOTE: Not every terminal emulator has a scrollback saving feature.

function bashrc_il_oslen() { local STRLEN="$(uname -o) $(uname -r) $(uname -p)"; echo ${#STRLEN}; }
function bashrc_il_newline() { echo -en "${BASHRC_IL_RESET}# "; }

function bashrc_print_infoline() {

if [ -z "$BASHRC_IS_LEGACY_MODE" ]; then # =================== True Color
    local BASHRC_IL_BG="\e[48;2;244;244;244m"         # very light gray
    local BASHRC_IL_FG="\e[38;2;142;142;142m"         # a bit darker than the above
    local BASHRC_IL_KEY="\e[1m\e[38;2;63;188;244m"    # light blue matching the material design theme "Azure"
    local BASHRC_IL_VALUE="\e[1m\e[38;2;125;25;142m"  # ubuntu purple
    local BASHRC_IL_NUM="\e[38;2;134;102;142m"        # light purple
else # ======================================================= 16 colors (tty)
    local BASHRC_IL_BG="\e[47m"                       # light gray
    local BASHRC_IL_FG="\e[1m"                        # white in tty
    local BASHRC_IL_KEY="\e[1m\e[38;2;63;188;244m"    # cyan in tty
    local BASHRC_IL_VALUE="\e[1m\e[38;2;125;25;142m"  # magenta in tty
    local BASHRC_IL_NUM="\e[1m"                       # white in tty
fi


local BASHRC_IL_RESET="\e[0m${BASHRC_IL_FG}${BASHRC_IL_BG}"

echo -en "$(bashrc_il_newline)${BASHRC_IL_KEY}Operating System:   ${BASHRC_IL_RESET} ${BASHRC_IL_VALUE}$(uname -o)${BASHRC_IL_RESET} ${BASHRC_IL_NUM}$(uname -r)${BASHRC_IL_RESET} [$(uname -m)]"
echo -e "$(bashrc_insert_separator " " $(($(bashrc_il_oslen) + 25)))"
echo -en "$(bashrc_il_newline)${BASHRC_IL_KEY}Shell:              ${BASHRC_IL_RESET} ${BASHRC_IL_VALUE}GNU/Bash${BASHRC_IL_RESET} ${BASHRC_IL_NUM}v$BASH_VERSION${BASHRC_IL_RESET}"
echo -e "$(bashrc_insert_separator " " $((${#BASH_VERSION} + 33)))"
echo -en "\e[0m" # RESET
}

bashrc_print_infoline

fi # END of $PS1, $PS2, $PS3, $PS4

# Begin to store previous commands into a file for minor $PS1 tweaks
# for performance and efficiency reasons, make sure this line is ALWAYS at the END of this file
[ ! -z "$PS1" ] && trap bashrc_write_lastcommand DEBUG
