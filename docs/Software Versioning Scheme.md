# Software Versioning Scheme


This document describes my software versioning scheme and how I version my software.

</br>

### Structure

```
n.nn-n(n...)
examples: 1.04-2, 1.29-12
```

The **first number** describes the main version of the software. This number becomes `1` when the software is feature complete and increases when future changes renders certain things incompatible to the previos version.

The **second number** is always two digits long and describes the progress in percent. For example when this number says `.30`, ~30% of the planed features for this version are  implemented.

The **third (and last) number** describes the revision. It is uses for small and trivial changes and bug fixes. This number can have any amount of digits.

</br>

--

### Note about NodeJS

NodeJS uses semantic versioning, which is incompatible with my versioning scheme. To workaround this, I use a mechanism to parse the version. The code can be found in the **tools** directory of this repository. (`NodeSwVersionParser`)

```
package.json
 version:      n.n(.)n    (examples: 1.0.4, 1.2.9)
 revision:     n(n...)    (          2,     12   )
```

- The *first number in the semantic version string* represents the main version.
- The *second number* represents the first digit of the progress number and the *third number* represents the second digit of the progress number. This change is required because `semver` is dropping all leading `0` from the numbers.

For the revision I add a custom JSON tag to the `package.json` file.
