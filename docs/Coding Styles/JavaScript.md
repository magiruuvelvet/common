# **JavaScript Coding Style**

This document describes my very special and unique JavaScript coding style. This coding style applies to NodeJS and Electron.

**NOTE:** Since I'm still learning JavaScript, this coding style guide is incomplete.

##### Why I don't use the common coding style everyone else is using?

This is simple to explain. The common JS coding style reminds me of clang format and I don't like this format. Its **too compact** and I can't read it. I'm a person which needs a lot of spacing to recognize the code better. When there is too much code compressed to minimum space, I'm lost :(


## General

### ── Naming Schemes

 - **Variables**: `all_lowercase`
 	- `global.` project globals
 	- `g_`      module globals, doesn't apply to `require`
 	- `s_`      constants, except object aliases
 	- `this.m_` object member variables

 - **Functions**: `all_lowercase`

 - **Classes**: `CamelCase` most of the time, in rare cases `all_lowercase` looks better. Object methods are always `CamelCase` ─ first letter can be either lower or upper case.

### ── Declarations

 - Imports and `require`s are always `const`, because  you should not modify or reassign those. You don't need to prepend a `g_` to the name, because this are not supposed to represent variables.
 - In `for...in` and `for...of` loops declare the variable always `const` and in regular `for` loops always `let`.

### ── Code Formatting

 - **Always** use line separators `;`, even when not needed!! It helps to read the code better.
 - `4` spaces intention, never tabs. Intent more as required, keep code which belongs together aligned between lines.
 - Try to avoid more than `100` columns per line. *Not strict*, there may be cases where it is okay to exceed this limit for optical reasons, but never go beyond `130` columns.
 - `{` and `}` are **always** in the next line. There are exceptions, in the following cases put curved brackets in the same line (don't forget the space):
   - when declaring enumerators
   - on `module.exports`
 - `[` are always at the same line as the declaration itself. `]` is in its own next line when declaring an array over multiple lines, otherwise in the same line.
 - Always use `//` comments and **never** `/**/` comments! There is one exception to that rule: It's ok when you use them on top of the file BEFORE the code starts. The reason for this is to quickly comment out whole code blocks with `/**/` for testing purposes and debugging.
 - Insert a space after keywords like, `if`, `while`, etc. In general, don't "glue" parentheses to anything else than function calls.
 - Don't insert a space after the `function` keyword. `function()`, `{` in the next line.
 - Lambda functions: `() =>`, `{` in the next line.
 - Don't use spaces inside parenthesis and brackets, except after commas. Example -> Correct: `[0]`, Wrong: `[ 0 ]`; Correct: `(i, n)`, Wrong: `( i, n )`
 - Always insert a blank line for `if-else` blocks. Don't "glue" `}` and `else (if)` together in the next line. It may take more space, but it improves the readability.
 - Use `while (true)` for infinite loops.

### ── File Formatting and Naming, Directory Management

 - Filenames and folder names are always `all_lowercase`. Try to prevent underscores as much as possible. Prefer dashes over underscores. </br> As an extend, this also applies to C++ files when adding native addons to the project.
