# **C++ Coding Style**

This document describes my unique C++ coding style.


## General

### ── Naming Schemes

 - **Variables**: `all_lowercase`
 	- `g_` globals
 	- `s_` statics
 	- `m_` class members (doesn't apply to `struct` and `union`)

 - **Functions**: mixed (`all_lowercase` or `CamelCase`)
 	- `all_lowercase`: for standalone global functions or members functions of a `struct-like class`.
 	- `CamelCase`: for member functions of classes, the first letter is **always** lowercase ─ sometimes I make exceptions to this rule. `:^)`

 - **Class**: always `CamelCase`. `public`, `protected` and `private` are always at the beginning of the line. Above this keywords must be an empty line (except under the class definition itself). Below this keyword can be an empty line if it improves general readability.
 - **Struct** (as type): always `all_lowercase`, suffix structs with `_t` to define them as a type. Structs are only used for data containers, where a simple variable isn't enough and a class would be overkill. Structs may never have member functions! As an exception you may use a class with `all_lowercase_t` naming instead.
 - **Struct** (as container): `all_lowercase` or `CamelCase`, can have a constructor, destructor and member functions. May **NOT** contain private and protected members.
 - **Union**: always `all_lowercase`, suffix unions with `_u`. No special rules apply to unions.

### ── Code Formatting

 - `4` spaces intention, never tabs. Intent more as required, keep code which belongs together aligned between lines.
 - Try to avoid more than `100` columns per line. *Not strict*, there may be cases where it is okay to exceed this limit for optical reasons, but never go beyond `130` columns.
 - `{` and `}` are **always** in next line, except for `enum` declarations the curved brackets must be in the same line as the declaration itself.
 - Always use `//` comments and **never** `/**/` comments! There is one exception to that rule: It's ok when you use them on top of the file BEFORE the code starts. The reason for this is to quickly comment out whole code blocks with `/**/` for testing purposes and debugging.
 - Never use `typedef`! Use `using my_alias_t = original_type;` instead.
 - Never use `using namespace`! It is ugly and can cause strange conflicts. Sometimes, in specific circumstances I use `using namespace` ─ in general it is better to use `using ...` instead.
 - Never use `this` unless there is a special reason to do so, the compiler is smart enough to choose the right member function or variable, especially if you follow the variable and function naming guidelines.
 - Insert a space after keywords like, `if`, `while`, etc. In general, don't "glue" parentheses to anything else than function or constructor calls. If you believe it or not, but this improves the overall readability of the code. `:^)`
 - Don't use spaces inside paranthesis and brackets, except after commas. Example -> Correct: `[0]`, Wrong: `[ 0 ]`; Correct: `(i, n)`, Wrong: `(i,n)`
 - Always insert a blank line for `if-else` blocks. Don't "glue" `}` and `else (if)` together in the next line. It may take more space, but it improves the readability.
 - Always `&reference`, never `& reference`.
 - Alawys `*pointer`, never `* pointer`.
 - When declaring pointers without assigning a address to it make them `nullptr` explicitly! **Always!** No exceptions.
 - Prefer `std::unique_ptr` over raw pointers whenever possible.
 - Prefer `std::weak_ptr` over raw pointers and use `.expired()` instead of `== nullptr` whenever possible.
 - Delete raw pointers if the object is no longer needed. Assign them `nullptr` after deletion, to ensure a address to `0x0` on all platforms and architectures.
 - Use range-based `for` loops when the index of something is not needed and a range-based `for` loop is possible.
 - Use `while (true)` for infinite loops.
 - **Never** use macros for constants, use `static const` instead. Don't forget the `s_` prefix.
 - **Never** use macros when an `inline` function is a possible alternative.
 - Use macros only when using an inline function isn't a good idea. Macros are nice to reduce typing or prevent code redundancy. Use common sense - in general macros doesn't look very nice, try to avoid them as much as possible.
 - Use the `auto` keyword wherever possible. Prefer [`auto&&`](http://stackoverflow.com/a/13242177/2039868) to preserve constness! </br> Allign as follows: `auto&& variable`.
 - Always use nested `namespace` declarations. `namespace My::Namespace`, it looks cleaner and nicer, and it's proper modern C++ code.
 - **NEVER EVER EVEN THINK ABOUT USING `goto`!!!**



 - When working with platform-specific code use `#if defined() - #elif defined() - #endif` blocks! If there is too many code at once, create platform-specific cpp files! Put your implementations in functions.

### ── File Formatting and Naming, Directory Management

 - Header files have the `*.hpp` extension. Source files have the `*.cpp` extension.
 - Header files which only holds preprocessor code have the `*.h` extension.
 - Filenames and folder names are `CamelCase`.
 - Always use include guards in header files. The name of the macro is the same as the filename in all UPPERCASE. Dots becomes `_`. Example: `MyAwesomeClass.hpp` -> `MYAWESOMECLASS_HPP`
 - `#pragma once` is forbidden, never use it. For that there are include guards. Personally, I never understood the use of this macro.
